import java.lang.Runtime;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.io.File;

public class Main {

    public static void main(String[] args) {

        System.out.println("Main: Java running in Docker...");
        //if a parameter is given, no object will be freed -> OutOfMemory
        boolean oom = false;
        if (args.length > 0) {
            System.out.println("Using OOM triggering mode!");
            oom = true;
        }
        System.out.println("-------------------------------");
        printRuntimeInfo();
        System.out.println("-------------------------------");

        pause(1200);

        List<String> objs = new ArrayList<>();

        long blocksize=20000;
        while(true) {
            Main.printMemoryInfo(objs.size());
            for (int i = 0; i < blocksize; i++) {
                objs.add(UUID.randomUUID().toString());
            }
            pause(500);

            if (!oom && objs.size() > (blocksize * 5)) {
                for (int i = 0; i < blocksize; i++) {
                    if (objs.size() > 0) {
                        objs.remove(0);
                    }
                }
            }
            pause(500);
        }
    }

    private static void pause(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            //nop
        }
    }

    public static void printRuntimeInfo() {
        Runtime runtime = Runtime.getRuntime();

        /* Total number of processors or cores available to the JVM */
        System.out.println("Available processors (cores): " + runtime.availableProcessors());

        /* Total amount of free memory available to the JVM */
        System.out.println("Free memory (bytes): " + runtime.freeMemory());

        /* This will return Long.MAX_VALUE if there is no preset limit */
        long maxMemory = runtime.maxMemory();
        /* Maximum amount of memory the JVM will attempt to use */
        System.out.println("Maximum memory (bytes): " +  (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemory));

        /* Total memory currently available to the JVM */
        System.out.println("Total memory available to JVM (bytes): " + runtime.totalMemory());

        /* Get a list of all filesystem roots on this system */
        File[] roots = File.listRoots();

        /* For each filesystem root, print some info */
        for (File root : roots) {
            System.out.println("File system root: " + root.getAbsolutePath());
            System.out.println("Total space (bytes): " + root.getTotalSpace());
            System.out.println("Free space (bytes): " + root.getFreeSpace());
            System.out.println("Usable space (bytes): " + root.getUsableSpace());
        }
    }

    public static void printMemoryInfo(long size) {
        Runtime runtime = Runtime.getRuntime();
        NumberFormat format = NumberFormat.getInstance();

        StringBuilder sb = new StringBuilder();
        long maxMemory = runtime.maxMemory();
        long allocatedMemory = runtime.totalMemory();
        long freeMemory = runtime.freeMemory();

        sb.append("allocated objects : " + size + System.lineSeparator());
        sb.append("free memory       : " + format.format(freeMemory / 1024) + System.lineSeparator());
        sb.append("allocated memory  : " + format.format(allocatedMemory / 1024) + System.lineSeparator());
        sb.append("max memory        : " + format.format(maxMemory / 1024) + System.lineSeparator());
        sb.append("total free memory : " + format.format((freeMemory + (maxMemory - allocatedMemory)) / 1024));

        System.out.println(sb.toString());
        System.out.println("-------------------------------");
    }
}
