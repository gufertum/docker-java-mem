# docker-java-mem

builds a simple java app, which uses up memory by allocating lots of string.
shows the memory consumption every second using stdout.

right now, this only works up to docker desktop 4.2.0, because the newer
versions do not respect the limits. see:

https://docs.docker.com/desktop/mac/release-notes/

- https://github.com/docker/for-mac/issues/6073
- https://github.com/docker/for-mac/issues/6093
```
Docker Desktop now uses cgroupv2. If you need to run systemd in a container then:
Ensure your version of systemd supports cgroupv2. It must be at least systemd 247. Consider upgrading any centos:7 images to centos:8.
Containers running systemd need the following options: --privileged --cgroupns=host -v /sys/fs/cgroup:/sys/fs/cgroup:rw.
```

## env vars

```
-XshowSettings:vm -XX:+UnlockExperimentalVMOptions -XX:+UseContainerSupport -XX:MaxRAMPercentage=75
```


## build it

```
./build.sh
```

## run it

```
./run.sh
```

## monitor it

the compose-file will run the image and you can check the memory consumption and limits
using `docker stats`.


